import tcp from "@SignalRGB/tcp";
import udp from "@SignalRGB/udp";
export function Name() { return "MagicHome"; }
export function Version() { return "1.0.0"; }
export function Type() { return "network"; }
export function Publisher() { return "WhirlwindFX"; }
export function Size() { return [1, 1]; }
export function DeviceType() { return "wifi"; }
/* global
discovery:readonly
controller:readonly
shutdownColor:readonly
LightingMode:readonly
forcedColor:readonly
RGBconfig:readonly
*/
export function ControllableParameters() {
	return [
		{property:"shutdownColor", group:"lighting", label:"Shutdown Color", description: "This color is applied to the device when the System, or SignalRGB is shutting down", min:"0", max:"360", type:"color", default:"#000000"},
		{property:"LightingMode", group:"lighting", label:"Lighting Mode", description: "Determines where the device's RGB comes from. Canvas will pull from the active Effect, while Forced will override it to a specific color", type:"combobox", values:["Canvas", "Forced"], default:"Canvas"},
		{property:"forcedColor", group:"lighting", label:"Forced Color", description: "The color used when 'Forced' Lighting Mode is enabled", min:"0", max:"360", type:"color", default:"#009bde"},
		{property:"RGBconfig", group:"lighting", label:"ARGB Channel Configuration", description: "Sets the RGB color order for the 5v ARGB Header. If you are experiencing issues, try switching to each one of these options until you find one which works", type:"combobox", values:["RGB", "RBG", "BGR", "BRG", "GBR", "GRB"], default:"RGB"},
	];
}

let deviceTCPServer;

const vLedNames = [ ];
const vLedPositions = [ ];

export function ledNames() {
	return vLedNames;
}

export function ledPositions() {
	return vLedPositions;
}

export function Initialize() {
	device.setName(`MagicHome: ${controller.id}`)
	device.setImageFromUrl(MagicHome.images[controller.model]);
	MagicHome.initTCPSocket();
}

export function Render() {
	MagicHome.sendRGB();
}

export function Shutdown(SystemSuspending) {
	const color = SystemSuspending ? "#000000" : shutdownColor;
	MagicHome.sendRGB(color);
}

function SetupChannel(ChannelsSize) {
	device.addChannel(`Channel 1`, ChannelsSize);
	device.channel(`Channel 1`).SetLedLimit(ChannelsSize);
	device.SetLedLimit(ChannelsSize);
}

// Not sure how segments will work, components engine is enough to make segments even with 1 channel, check in future
function SetupChannels(Channels, ChannelsSize) {
	let DeviceLedLimit = 0;
	const oldChannels = device.getChannelNames().length;

	if(Channels > oldChannels){
		// Update existing channels
		for(let i = 0; i < oldChannels; i++) {
			device.channel(`Channel ${i + 1}`).SetLedLimit(ChannelsSize);
			DeviceLedLimit += ChannelsSize;
		}

		// Create new channels 
		for(let i = oldChannels ; i < Channels; i++) {
			device.addChannel(`Channel ${i + 1}`, ChannelsSize);
			DeviceLedLimit += ChannelsSize;
		}
	}

	if(Channels == oldChannels){
		// Update existing channels
		for(let i = 0; i < oldChannels; i++) {
			device.channel(`Channel ${i + 1}`).SetLedLimit(ChannelsSize);
			DeviceLedLimit += ChannelsSize;
		}
	}

	if(Channels < oldChannels){
		// Update existing channels
		for(let i = 0; i < Channels; i++) {
			device.channel(`Channel ${i + 1}`).SetLedLimit(ChannelsSize);
			DeviceLedLimit += ChannelsSize;
		}

		// Remove ghost channels
		for(let i = Channels; i < oldChannels; i++) {
			device.removeChannel(`Channel ${i + 1}`);
			device.pause(500)
		}
	}

	device.SetLedLimit(DeviceLedLimit);
}

function hexToRgb(hex) {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	const colors = [];
	colors[0] = parseInt(result[1], 16);
	colors[1] = parseInt(result[2], 16);
	colors[2] = parseInt(result[3], 16);

	return colors;
}

// -------------------------------------------<( Discovery Service )>--------------------------------------------------

let UDPServer;
let TCPServer;

class MagicHomeProtocol {
	constructor() {
		this.config = {
			RGBOrder: 0
		};

		this.token = "";
		this.isInDirectMode = false;
		this.isInitialized = false;

		// Source: https://community.homey.app/t/app-pro-magic-home-led/1750/302?page=16
		this.models = [
			'AK001-ZJ100',
			'AK001-ZJ200',
			'AK001-ZJ210',
			'AK001-ZJ2101',
			'AK001-ZJ2104',
			'AK001-ZJ2145',
			'AK001-ZJ2147',
			'AK001-ZJ2148',
			'AK001-ZJ21410'
		];

		this.images = {
			'AK001-ZJ100'  : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ200'  : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ210'  : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ2101' : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ2104' : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ2145' : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ2147' : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ2148' : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
			'AK001-ZJ21410'  : "https://assets.signalrgb.com/devices/brands/magichome/misc/ak001-zj2148.png",
		};

		this.RGBOrder = {
			0: "RGB",
			1: "RBG",
			2: "GRB",
			3: "GBR",
			4: "BRG",
			5: "BGR",
		}
	}

	getLEDs() { return this.config.LEDs;}
	setLEDs(leds) { this.config.LEDs = leds; }

	getRGBOrder() { return this.config.RGBOrder;}
	setRGBOrder(Order) { this.config.RGBOrder = Order; }

	initTCPSocket() {
		if(deviceTCPServer === undefined) {
			device.log("Device TCP Server not initialized...")
			deviceTCPServer = new TcpSocketServer(controller.ip, 5577);
			deviceTCPServer.start();
		}
	}

	/** Fetch the device status */
	fetchStatus() {
		/*
			Type 1
			State 2 = 0x23 (on) || 0x24 (off)
			Mode 3

			R 6
			G 7
			B 8
			W 9
			Firmware 10

			0x63, 0x12, 0x21, 0x36

			pixel 2
			segments 5
			type 6
			rgb order 7

		
		 81 a3 23 61 01 32 00 64 00 00 01 00 78 b8

		*/

		device.log("Fetching status...")
		const packet = [0x81, 0x8a, 0x8b];
		packet[packet.length] = this.CalculateCrc(packet);

		deviceTCPServer.sendPacket(packet);
	}

	fetchLEDData() {

		device.log("Fetching LED Data...")
		const packet = [0x63, 0x12, 0x21, 0x36];
		packet[packet.length] = this.CalculateCrc(packet);

		deviceTCPServer.sendPacket(packet);
	}

	/** Turn the device on or off.*/
	setDevicePower(on) {
		/*
		on:    []byte{0x71, 0x23, 0x94},
		off:   []byte{0x71, 0x24, 0x95},
		*/
	}

	/** Set the device colors while in direct mode and individually addressing leds.*/
	sendRGB(overrideColor) {

		//Fetch Colors
		let ChannelLedCount = device.channel(`Channel 1`).LedCount();
		const componentChannel = device.channel(`Channel 1`);

		let RGBData = [];
		let packet  = [];

		if(LightingMode === "Forced"){
			RGBData = device.createColorArray(forcedColor, ChannelLedCount, "Inline", RGBconfig);
		}else if(componentChannel.shouldPulseColors()){
			const pulseColor = device.getChannelPulseColor(`Channel 1`);
			const pulseCount = device.channel(`Channel 1`).LedLimit();
			RGBData = device.createColorArray(pulseColor, pulseCount, "Inline", RGBconfig);
		}else if(overrideColor){
			RGBData = device.createColorArray(overrideColor, ChannelLedCount, "Inline", RGBconfig);
		}else{
			RGBData = device.channel(`Channel 1`).getColors("Inline", RGBconfig);
		}

		//packet = [0x59, ((ChannelLedCount * 3) + 9) >> 8, ((ChannelLedCount * 3) + 9) & 0xFF].concat(RGBData).concat([0x00, ChannelLedCount, 0x01, 0x64, 0x00, 0x00]);
		//packet[packet.length] = this.CalculateCrc(packet); // It seems to not care about CrC :kekw:

		packet = [
			0xb0, 0xb1, 0xb2, 0xb3, 0x00, 0x01, 0x01, 0x64, 0x2f, 0xac, 0xed, 0x2f, 0xac, 0xed,
			0xb0, 0xb1, 0xb2, 0xb3, 0x00, 0x01, 0x01, 0x02, 0x2f, 0xac, 0xed, 0x2f, 0xac, 0xed];

		deviceTCPServer.sendPacket(packet);
		device.pause(10)
	}

	/** Send a packet whilst ensuring we have an open tcp Server */
	sendControl(packet) {
		if(deviceTCPServer === undefined) {
			device.log("Device TCP Server not initialized...")
			deviceTCPServer = new TcpSocketServer(controller.ip, 5577);
			deviceTCPServer.start();
		}

		deviceTCPServer.sendPacket(packet);
	}

	CalculateCrc(packet) {
		let iCrc = 0;

		for (let iIdx = 0; iIdx < packet.length; iIdx++) {
			iCrc += packet[iIdx];
		}

		return iCrc.toString(16).slice(-2);
	}
}

const MagicHome = new MagicHomeProtocol();

export function DiscoveryService() {
	this.IconUrl = "https://assets.signalrgb.com/brands/magichome/logo.png";

	this.firstRun = true;
	this.UdpBroadcastAddress	= "255.255.255.255";
	this.UdpBroadcastPort		= 48899;
	this.UdpListenPort			= 0;
	this.UdpBroadcastMessage	= "HF-A11ASSISTHREAD";

	this.lastPollTime = 0;
	this.PollInterval = 30 * 1000;

	this.cache = new IPCache();
	this.activeSockets = new Map();
	this.activeSocketTimer = Date.now();

	this.Initialize = function(){
		service.log("Initializing Plugin!");
		service.log("Searching for network devices...");
		this.LoadCachedDevices();
	};

	this.Discovered = function(value) {
		if(!this.cache.Has(value.ip)){
			if(value.port == this.UdpBroadcastPort) {
				service.log(`MagicHome device at ${value.ip}!`);
	
				this.CreateControllerDevice(value);
			}
		}
	};

	this.Update = function(){
		for(const cont of service.controllers){
			cont.obj.update();
		}
		
		//this.clearSockets();
		this.CheckForDevices();
	};

	this.CreateControllerDevice = function(value){

		service.log("Controller: " + value.response);

		let valueResponse	= value.response.toString().split(",");
		value.id			= valueResponse[1];
		value.model			= valueResponse[2];
		value.mac			= valueResponse[1].match( /.{1,2}/g ).join( ':' );
		value.paired		= false;

		if(MagicHome.models.includes(valueResponse[2])){
			if(this.cache.Has(value.ip)){
				service.log("Device found in cache, updating controller!")
				const cachedController = this.cache.Get(value.ip)
				const controller = service.getController(cachedController.id);
				
				if(controller === undefined){
					service.log("Device controller not found, creating controller!")
					service.addController(new MagicHomeController(value));
				}else{
					controller.updateWithValue(value);
				}
	
			}else{
				service.log("Device not found in cache, creating controller!")
				service.addController(new MagicHomeController(value));
			}
		}else{
			service.log("Model not supported yet: " + valueResponse[2]);
		}
	};

	this.LoadCachedDevices = function(){
		service.log("Loading Cached Devices...");

		for(const [key, value] of this.cache.Entries()){
			service.log(`Found Cached Device: [${key}: ${JSON.stringify(value)}]`);
			this.checkCachedDevice(value.ip);
		}
	};

	this.FetchCachedDevices = function(){
		return this.cache.Entries();
	};

	this.checkForcedIP = function(ipAddress) {
		service.log(`Checking IP: ${ipAddress}`);

		if(UDPServer !== undefined) {
			UDPServer.stop();
			UDPServer = undefined;
		}

		const socketServer = new UdpSocketServer(ipAddress, 48899, true);
		this.activeSockets.set(ipAddress, socketServer);
		socketServer.start();
	};

	this.checkCachedDevice = function(ip) {
		service.log(`Checking IP: ${ip}`);

		if(UDPServer !== undefined) {
			UDPServer.stop();
			UDPServer = undefined;
		}

		const socketServer = new UdpSocketServer(ip, 48899, true);
		this.activeSockets.set(ip, socketServer);
		this.activeSocketTimer = Date.now();
		socketServer.start();
	};

	this.clearSockets = function() {
		if(Date.now() - this.activeSocketTimer > 15000 && this.activeSockets.size > 0) {
			service.log("Nuking Active Cache Sockets.");

			for(const [key, value] of this.activeSockets.entries()){
				service.log(`Nuking Socket for IP: [${key}]`);
				value.stop();
				this.activeSockets.delete(key);
				//Clear would be more efficient here, however it doesn't kill the socket instantly.
				//We instead would be at the mercy of the GC.
			}
		}
	};

	this.getSocket= function(key) {
		return this.activeSockets.get(key);
	};

	this.link = function(controllerObj){
		service.log(`Pairing controller: ${JSON.stringify(controllerObj)}`);

		const cachedController = this.cache.Get(controllerObj.ip)
		const controller = service.getController(cachedController.id);

		controller.paired = true;

		service.updateController(controller);
		service.announceController(controllerObj);
	}

	this.unlink = function(controllerObj) {
		service.log(`Unpairing controller: ${JSON.stringify(controllerObj)}`);

		service.log(`Stopping TCP Socket for ${controllerObj.ip}`);
		const tcpSocket = this.getSocket(controllerObj.ip);
		if(tcpSocket){
			tcpSocket.stop();
			this.activeSockets.delete(controllerObj.ip);
		}

		const cachedController = this.cache.Get(controllerObj.ip)
		const controller = service.getController(cachedController.id);

		controller.paired = false;

		service.updateController(controller);
		service.suppressController(controller);
	}

	this.remove = function(controllerObj = false) {

		if (controllerObj) {
			service.log(`Current controller: ${JSON.stringify(controllerObj)}`);

			service.log(`Stopping TCP Socket for ${controllerObj.ip}`);
			const tcpSocket = this.getSocket(controllerObj.ip);
			if(tcpSocket){
				tcpSocket.stop();
				this.activeSockets.delete(controllerObj.ip);
			}

			service.log(`Removing from cache: ${controllerObj.ip}`);
			this.cache.Remove(controllerObj.ip)
			
			service.log(`Removing controller: ${controllerObj.id}`);
			service.removeController(controllerObj);

			const cachedDevices = this.cache.Entries()
			console.log(cachedDevices);
		} else {
			this.cache.PurgeCache();
			const cachedDevices = this.cache.Entries()
			console.log(cachedDevices);
	
			for(const [key, value] of cachedDevices){
				service.log(`Removing Cached Device: [${key}: ${JSON.stringify(value)}]`);
				service.removeController(value);
			}

			this.cache.DumpCache();
		}
	};

	this.CheckForDevices = function(){
		if(Date.now() - discovery.lastPollTime < discovery.PollInterval){
			return;
		}

		discovery.lastPollTime = Date.now();
		service.log("Broadcasting device scan...");
		service.broadcast(discovery.UdpBroadcastMessage);
	};

	this.ResponseStringToObj = function(sResponse) {
		const sResp = sResponse.toString().split("\r\n");
		const obj = {};
		sResp.forEach(function(property) {
			const tup = property.split(':');
			obj[tup[0]] = tup[1];
		});

		return obj;
	};

}

class MagicHomeController{
	constructor(value){
		this.updateWithValue(value);
		this.initialized = false;

		this.cacheControllerInfo(this);
	}

	updateWithValue(value){
		this.id = value?.id ?? "Unknown ID";
		this.port = value?.port ?? 5577;
		this.ip = value?.ip ?? "Unknown IP";
		this.model = value?.model ?? "Unknown Model";
		this.mac = value?.mac ?? "Unknown MAC";
		this.paired = value?.paired ?? false;

		service.updateController(this);
	}

	update(){
		if(!this.initialized){
			this.initialized = true;

			service.updateController(this);
		}
	}

	cacheControllerInfo(value){
		discovery.cache.Add(value.ip, {
			port: value.port,
			ip: value.ip,
			id: value.id,
			model: value.model,
			mac: value.mac,
			paired: value.paired
		});
	}

}

class UdpSocketServer{
	constructor (ip, port, isDiscoveryServer = false) {
		this.count = 0;
		/** @type {udpSocket | null} */
		this.server = null;
		this.listenPort = 0;
		this.broadcastPort = port;
		this.ipToConnectTo = ip;
		this.isDiscoveryServer = isDiscoveryServer;

		this.log = (msg) => { this.isDiscoveryServer ? service.log(msg) : device.log(msg); };
	}

	sendPacket(packet) {
		this.server.send(packet);
	}

	start(){
		this.server = udp.createSocket();

		if(this.server){

			// Given we're passing class methods to the server, we need to bind the context (this instance) to the function pointer
			this.server.on('error', this.onError.bind(this));
			this.server.on('message', this.onMessage.bind(this));
			this.server.on('listening', this.onListening.bind(this));
			this.server.on('connection', this.onConnection.bind(this));
			this.server.bind(this.listenPort);
			this.server.connect(this.ipToConnectTo, this.broadcastPort);
		}
	};

	stop(){
		if(this.server) {
			this.server.disconnect();
			this.server.close();
		}
	}

	onConnection(){
		const deviceData = this.server.remoteAddress();

		this.log('Connected to remote UDP socket!');
		this.log("UDP Remote Address: " + deviceData.address);

		if(this.isDiscoveryServer) {
			this.log("Discovery UDP fetch status...")
			this.sendPacket("HF-A11ASSISTHREAD");
		}
	};

	onListenerResponse(msg) {
		this.log('UDP Socket Data received from client on Listener');
		this.log(msg);
	}

	onListening(){
		const address = this.server.address();
		this.log(`UDP Socket Server is listening at port ${address.port}`);

		// Check if the socket is bound (no error means it's bound but we'll check anyway)
		this.log(`UDP Socket Bound: ${this.server.state === this.server.BoundState}`);
	};
	onMessage(msg){
		this.log('UDP Socket Data received from client');
		this.log(msg);

		if(this.isDiscoveryServer) {

			if(msg.port === 48899) {
				this.log('MagicHome response from the device!');
	
				if(discovery.cache.Has(msg.address)){
					// udate cache and controller
					this.log("Device found in cache. Creating controller with cached data...")
					let value = discovery.cache.Get(msg.address)
					service.addController(new MagicHomeController(value));
					discovery.link(value)
					this.server.close();
				}else{
					this.log("Device not found in cache.")
	
					let valueResponse = msg.data.toString().split(",");
					msg.id			= valueResponse[1];
					msg.ip			= msg.address;
					msg.response	= msg.data;
					msg.port		= 5577;
		
					discovery.CreateControllerDevice(msg);
					this.server.close();
				}
			} else {
				this.log(`MagicHome port not found at ${msg.address}!`);
				this.server.close();
			}

		}

	};
	onError(code, message){
		this.log(`UDP Socket Error: ${code} - ${message}`);
		this.server.close(); // We're done here
	};
}

class TcpSocketServer{
	constructor (ip, port, isDiscoveryServer = false) {
		this.count = 0;
		/** @type {tcpSocket | null} */
		this.server = null;
		this.listenPort = 0;
		this.broadcastPort = port;
		this.ipToConnectTo = ip;
		this.isDiscoveryServer = isDiscoveryServer;
		this.IDToCheckFor = 0;
		this.connected	= false;

		this.responseCallbackFunction = (msg) => { this.log("No Response Callback Set Callback cannot function"); msg; };

		this.log = (msg) => { this.isDiscoveryServer ? service.log(msg) : device.log(msg); };
	}

	setIDToCheckFor(ID) {
		this.IDToCheckFor = ID;
	}

	getIDToCheck() { return this.IDToCheckFor; }

	setCallbackFunction(responseCallbackFunction) {
		this.responseCallbackFunction = responseCallbackFunction;
	}

	sendPacket(packet) {
		//this.log("Sending packet: " + packet)

		this.server.send(packet);
	}

	start(){
		this.server = tcp.createSocket();

		if(this.server){

			// Given we're passing class methods to the server, we need to bind the context (this instance) to the function pointer
			this.server.on('error', this.onError.bind(this));
			this.server.on('message', this.onMessage.bind(this));
			this.server.on('listening', this.onListening.bind(this));
			this.server.on('connection', this.onConnection.bind(this));
			this.server.bind(this.listenPort);
			this.server.connect(this.ipToConnectTo, this.broadcastPort);
		}
	};

	stop(){
		if(this.server) {
			this.server.disconnect();
			this.server.close();
		}
	}

	onConnection(){
		const deviceData = this.server.remoteAddress();

		this.log('Connected to remote TCP socket!');
		this.log("TCP Remote Address: " + deviceData.address);
		this.connected = true;

		//MagicHome.fetchStatus();
		MagicHome.fetchLEDData();
	};

	onListenerResponse(msg) {
		this.log('TCP Data received from client on Listener');
		this.log(msg);
	}

	onListening(){
		const address = this.server.address();
		this.log(`TCP Server is listening at port ${address.port}`);

		// Check if the socket is bound (no error means it's bound but we'll check anyway)
		this.log(`TCP Socket Bound: ${this.server.state === this.server.BoundState}`);
	};
	
	onMessage(msg){
		this.log('TCP Data received from client');

		if(msg.data.length == 14) {
			const bufferData = new Uint8Array(msg.buffer);

			this.log("Model: " + bufferData[1])
			this.log(bufferData[2] === 0x23 ? "Device is: on" : "Device is: off")
			this.log("Firmware: " + bufferData[10])
		}else if(msg.data.length == 11) {
			const bufferData = new Uint8Array(msg.buffer);

			this.log("Zones: " + bufferData[5])
			this.log("LEDs: " + bufferData[3])

			MagicHome.setRGBOrder(MagicHome.RGBOrder[bufferData[7]])
			this.log("Controller RGB order: " + MagicHome.getRGBOrder())

			SetupChannel(bufferData[3])
		}else {
			this.log("Not a valid packet length")
		}
	};
	onError(code, message){
		this.log(`Error: ${code} - ${message}`);
		//this.server.close(); // We're done here
	};
}

class IPCache{
	constructor(){
		this.cacheMap = new Map();
		this.persistanceId = "ipCache";
		this.persistanceKey = "cache";

		this.PopulateCacheFromStorage();
	}
	Add(key, value){

		service.log(`Adding ${key} to IP Cache...`);

		this.cacheMap.set(key, value);
		this.Persist();
	}

	Remove(key){
		this.cacheMap.delete(key);
		this.Persist();
	}
	Has(key){
		return this.cacheMap.has(key);
	}
	Get(key){
		return this.cacheMap.get(key);
	}
	Entries(){
		return this.cacheMap.entries();
	}

	PurgeCache() {

		for(const [key, value] of this.Entries()){
			service.log(`Found Cached Device: [${key}: ${JSON.stringify(value)}]`);
			this.Remove(value.ip);
			discovery.remove(value)
		}

		service.removeSetting(this.persistanceId, this.persistanceKey);
		service.log("Purging IP Cache from storage!");
	}

	PopulateCacheFromStorage(){
		service.log("Populating IP Cache from storage...");

		const storage = service.getSetting(this.persistanceId, this.persistanceKey);

		if(storage === undefined){
			service.log(`IP Cache is empty...`);

			return;
		}

		let mapValues;

		try{
			mapValues = JSON.parse(storage);
		}catch(e){
			service.log(e);
		}

		if(mapValues === undefined){
			service.log("Failed to load cache from storage! Cache is invalid!");

			return;
		}

		if(mapValues.length === 0){
			service.log(`IP Cache is empty...`);
		}

		this.cacheMap = new Map(mapValues);
	}

	Persist(){
		service.log("Saving IP Cache...");
		service.saveSetting(this.persistanceId, this.persistanceKey, JSON.stringify(Array.from(this.cacheMap.entries())));
		this.DumpCache();
	}

	DumpCache(){
		service.log("Current cache: ");
		for(const [key, value] of this.cacheMap.entries()){
			service.log([key, value]);
		}
	}
}
